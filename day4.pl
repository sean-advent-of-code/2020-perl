#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my @input = ();
foreach my $line (<>)
{
  push @input, $line;
}

my $previousEnter = 0;
my $currentString = "";
my @squashed = ();
for (my $i = 0; $i <= $#input; $i++)
{
  my $line = $input[$i];
  chomp($line);
  if ($line =~ /\S+.*/)
  {
    $currentString = $currentString . " " . $line;
  } else
  {
    push @squashed, $currentString;
    $currentString = "";
  }
}
push @squashed, $currentString;

my $count = 0;

foreach (@squashed)
{
  # print("$_\n");
  my %hash = $_ =~ /(\S+)\s*:\s*(\S+)/g;
  if (exists $hash{"byr"} &&
      exists $hash{"iyr"} &&
      exists $hash{"eyr"} &&
      exists $hash{"hgt"} &&
      exists $hash{"hcl"} &&
      exists $hash{"ecl"} &&
      exists $hash{"pid"})
  {
    if ($hash{"byr"} > 1919 && $hash{"byr"} < 2003)
    {
      if ($hash{"iyr"} > 2009 && $hash{"iyr"} < 2021)
      {
        if ($hash{"eyr"} > 2019 && $hash{"eyr"} < 2031)
        {
          if ($hash{"hcl"} =~ /^#[0-9a-f]{6}$/)
          {
            if ($hash{"ecl"} =~ /(amb)|(blu)|(brn)|(gry)|(grn)|(hzl)|(oth)/)
            {
              if ($hash{"pid"} =~ /^[0-9]{9}$/)
              {
                if ($hash{"hgt"} =~ /^(\d+)cm$/)
                {
                  if ($1 > 149 && $1 < 194)
                  {
                    $count++;
                  }
                } elsif ($hash{"hgt"} =~ /^(\d+)in$/)
                {
                  if ($1 > 58 && $1 < 77)
                  {
                    $count++;
                  }
                }
              }
            }
          }
        }
      }
    }
    print("$_  count=$count \n")
  }
}

print("answer is $count\n");