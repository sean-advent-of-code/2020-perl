package day2;
use strict;
use warnings FATAL => 'all';



my $validPasswords = 0;
foreach my $line (<>)
{
  my @split = split / /, $line;
  my @numbers = split /-/, $split[0];
  $split[1] =~ s/://g; # remove the trailing colon

  if (( substr($split[2], $numbers[0]-1, 1) eq $split[1]) ^ (substr($split[2], $numbers[1]-1, 1) eq $split[1]))
  {
    $validPasswords++;
  }


}

print("\nAnswer is: $validPasswords\n\n")
