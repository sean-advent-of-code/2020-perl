package day3;
use strict;
use warnings FATAL => 'all';

sub treesHit
{
  my @forest = @{$_[0]};
  my $right = $_[1];
  my $down = $_[2];

  my $treesHit = 0;
  my $posRight = 1;

  for(my $i = 0; $i <= $#forest; $i+=$down)
  {
    my $line = $forest[$i];
    chomp($line);
    print "$line\t";
    my $offset = $posRight % length($line);
    if (substr($line, $offset - 1, 1) eq "#")
    {
      $treesHit++;
      substr($line, $offset - 1, 1) = "X";
    } else
    {
      substr($line, $offset - 1, 1) = "O";
    }
    print "$line\n";
    $posRight += $right;
  }
  return $treesHit;
}

my @inputForest = ();
foreach my $line (<>)
{
  push @inputForest, $line;
}


my $right1Down1 = treesHit(\@inputForest, 1, 1);
print "\n Number of trees hit is: $right1Down1 \n\n";
my $right3Down1 = treesHit(\@inputForest, 3, 1);
print "\n Number of trees hit is: $right3Down1 \n\n";
my $right5Down1 = treesHit(\@inputForest, 5, 1);
print "\n Number of trees hit is: $right5Down1 \n\n";
my $right7Down1 = treesHit(\@inputForest, 7, 1);
print "\n Number of trees hit is: $right7Down1 \n\n";
my $right1Down2 = treesHit(\@inputForest, 1, 2);
print "\n Number of trees hit is: $right1Down2 \n\n";

my $answer = $right1Down1 * $right3Down1 * $right5Down1 * $right7Down1 * $right1Down2;
print "\n Answer is: $answer \n\n";
