#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my @input = ();

foreach my $line (<>)
{
  push @input, $line;
}

foreach my $val1 (@input)
{
  foreach my $val2 (@input)
  {
    foreach my $val3 (@input)
    {
      if ($val1 + $val2 + $val3 == 2020)
      {
        my $answer = $val1 * $val2 * $val3;
        print($answer);
        print("\n");
      }
    }
  }
}