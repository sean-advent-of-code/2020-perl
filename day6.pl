package day3;
use strict;
use warnings FATAL => 'all';


my @input = ();
foreach my $line (<>)
{
  push @input, $line;
}

my $previousEnter = 0;
my $currentString = "";
my @squashed = ();
my $newVar = 1;
for(my $i = 0; $i <= $#input; $i++)
{
  my $line = $input[$i];
  chomp($line);
  if ($line =~ /\S+.*/) {
      if ($newVar){
        $currentString = $line;
        $newVar = 0;
      }else 
      {
        $currentString =~ s/[^$line]//g
      }
      
  }else {
      push @squashed, $currentString;
      $currentString = "";
        $newVar = 1;
  }
}
push @squashed, $currentString;
my $total = 0;
foreach (@squashed){
    $_ =~ s/(.)(?=.*?\1)//g;
    $total += length($_);
    print "$_\n";
}
    print "Answer is $total";

