#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';

my $numberOfBits = 10;
my $highestSeatID = 2**$numberOfBits;
my $sum = ($highestSeatID * ($highestSeatID + 1)) / 2;
my $maxID = 0;
my $minID = $highestSeatID;


foreach my $line (<>)
{
  my $currentID = 0;

  if ($line =~ /([BF]{7})([RL]{3})/)
  {
    foreach (split //, $1)
    {
      $currentID = $currentID << 1;
      if ($_ eq 'B')
      {
        $currentID = $currentID | 1;
      }
    }
    foreach (split //, $2)
    {
      $currentID = $currentID << 1;
      if ($_ eq 'R')
      {
        $currentID = $currentID | 1;
      }
    }
    if ($maxID < $currentID)
    {
      $maxID = $currentID;
    }
    if ($minID > $currentID)
    {
      $minID = $currentID;
    }
    $sum -= $currentID;
  }
}

$sum -= (($minID - 1) * $minID ) / 2;
$sum -= (($highestSeatID * ($highestSeatID +1))/2) -  (($maxID  * ($maxID + 1 )) / 2);

print("seatID is: $sum\n");


